<?php

class ReportDAO {
    private $id_report;
    private $date;
    private $new_cases;
    private $cumulative_cases;
    private $new_deaths;
    private $cumulative_deaths;
    private $id_country_country;	
    
    function ReportDAO($pId_report, $pDate, $pNew_cases, $pCumulative_cases, $pNew_deaths, $pCumulative_deaths, $pId_country_country) {
        
        $this -> id_report = $pId_report;
        $this -> date = $pDate;
        $this -> new_cases = $pNew_cases;
        $this -> cumulative_cases = $pCumulative_cases;
        $this -> new_deaths = $pNew_deaths;
        $this -> cumulative_deaths = $pCumulative_deaths;
        $this -> id_country_country = $pId_country_country;
    }
    
    function consultaID($param) {
        return "SELECT DISTINCT
                    id_report,
                    date AS fecha,
                    new_cases,
                    cumulative_cases,
                    new_deaths,
                    cumulative_deaths,
                    id_country_country
                FROM
                    report
                WHERE
                   id_country_country = 'DZ' 
                   AND date =(
                    SELECT
                        MAX(date)
                    FROM
                        report
                );";
    }
    
}

?>