<?php 
    $paises = new Country();
    $pais = $paises -> buscaTodo();
    /*
     $(document).ready(function(){
    	$("#filtro").keyup(function(){
    		if($("#filtro").val().length > 0){
    			alert($("#filtro").val());
    			url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/mostrarDatosAjax.php") ?>&idPais=" + $("#filtro").val();
    			$("#result").load(url);
    		}
    	});
    });
     * */
?>

<div class="container">
	<div class="row mt-8">
		<div class="col-9 text-center">
			<h1>COVID</h1>
		</div>
	</div>

	<div class="row mt-3">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3>Proyecto covid</h3>
				</div>
				<div class="card-body">
					<div class="input-group">
						<div class="input-group-prepend">
							<label class="input-group-text">Edition</label>
						</div>
						<select class="custom-select" id="pais">
							<?php
								echo "<option disabled selected>--Seleccione pais--</option>";
                                foreach ($pais as $paisActual) {
                                    echo "<option value='" . $paisActual -> getId_country() . "'>" . $paisActual->getName() . "</option>";
                                }
                            ?>
						</select>
					</div>
					<div class="row mt-3">
						<div class="col">
							<div id="result"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script>
$(document).ready(function(){
	$("#pais").change(function() {
		if($("#pais").val().length > 0){	
			url = "indexAjax.php?pid=<?php echo  base64_encode("presentacion/mostrarDatosAjax.php") ?>&idPais=" + $("#pais").val();
			$("#result").load(url);
		}
	});
});
</script>