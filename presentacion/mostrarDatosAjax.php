<?php
    $filtro = $_GET["idPais"];
    $paises = new Report();
    $reporte = $paises -> consultaID($filtro);
?>

<body class="fondo-loging" >
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Regitro</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>	
								<th scope="col">Codigo</th>							
								<th scope="col">Region</th>
								<th scope="col">Nombre</th>
								<th scope="col">Casos Acumulados</th>
								<th scope="col">Muertes Acumuladas</th>
								<th scope="col">Casos Ultimo Dia</th>
								<th scope="col">Muertes Ultimo Dia</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($reporte as $paisesActual){
							    echo "<tr>";
							    echo "<td>" . $paisesActual -> getId_report()  . "</td>";
							    echo "<td>" . $paisesActual -> getId_country_country() -> getName() . "</td>";		
							    echo "<td>" . $paisesActual -> getId_country_country() -> getId_region_region() -> getName() . "</td>";
							    echo "<td>" . $paisesActual -> getCumulative_cases() . "</td>";
							    echo "<td>" . $paisesActual -> getCumulative_deaths() . "</td>";
							    echo "<td>" . $paisesActual -> getNew_cases() . "</td>";
							    echo "<td>" . $paisesActual -> getNew_deaths() . "</td>";						    
							    echo "</tr>";							    
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>