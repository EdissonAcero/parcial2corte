<?php 
require_once "persistencia/Conexion.php";
require_once 'persistencia/RegionDAO.php';

class Region {
    
    private $id_region;
    private $name;
    private $conecxion;
    private $regionDAO;
    
    public function getId_region()
    {
        return $this->id_region;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    function Region($pIid_region="", $pName="") {
        $this->id_region = $pIid_region;
        $this->name = $pName;
        $this->conecxion = new Conexion();
        $this->regionDAO = new RegionDAO($pIid_region, $pName);
    }

    function consultaID($param) {
        $this -> conecxion -> abrir();
        $this -> conecxion -> ejecutar($this -> regionDAO -> consultaDato($param));
        $this -> conecxion -> cerrar();
        $resultado = $this -> conecxion -> extraer()[0];
        $this -> name = $resultado;
    }
}
?>
