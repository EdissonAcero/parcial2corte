<?php 
require_once 'persistencia/CountryDAO.php';
require_once "persistencia/Conexion.php";

class Country {
    private $id_country;
    private $name;
    private $id_region_region;
    private $conexion;
    private $countryDAO;
    
    public function getId_country()
    {
        return $this->id_country;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getId_region_region()
    {
        return $this->id_region_region;
    }

    function Country($pId_country = "", $pName = "", $pId_region_region = "") {
        $this->id_country = $pId_country;
        $this->name = $pName;
        $this->id_region_region = $pId_region_region;
        $this->conexion = new Conexion();
        $this->countryDAO = new CountryDAO($pId_country, $pName, $pId_region_region);
    }
    
    function buscaTodo() {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->countryDAO->buscaTodo());
        $this->conexion->cerrar();
        $paises = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($paises, new Country($resultado[0], $resultado[1], $resultado[2]));
        }
        return $paises;
    }
    
    function buscaID($id) {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> countryDAO -> buscaID($id));
        
        $registro = $this -> conexion -> extraer();
        
        $this -> name = $registro[0];
        
        $region = new Region();
        $region -> consultaID($registro[1]);
        $this -> id_region_region = $region;
        
        $this -> conexion -> cerrar();
    }
}


?>