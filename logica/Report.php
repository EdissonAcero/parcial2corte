<?php
require_once 'persistencia/ReportDAO.php';
require_once "persistencia/Conexion.php";

class Report {
    private $id_report;
    private $date;
    private $new_cases;
    private $cumulative_cases;
    private $new_deaths;
    private $cumulative_deaths;
    private $id_country_country;
    private $conexion;
    private $reportDAO;
    public function getId_report()
    {
        return $this->id_report;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getNew_cases()
    {
        return $this->new_cases;
    }

    public function getCumulative_cases()
    {
        return $this->cumulative_cases;
    }

    public function getNew_deaths()
    {
        return $this->new_deaths;
    }

    public function getCumulative_deaths()
    {
        return $this->cumulative_deaths;
    }

    public function getId_country_country()
    {
        return $this->id_country_country;
    }

    function Report($pId_report = "", $pDate = "", $pNew_cases = "", $pCumulative_cases = "", $pNew_deaths = "", $pCumulative_deaths = "", $pId_country_country = "") {
        $this -> id_report = $pId_report;
        $this -> date = $pDate;
        $this -> new_cases = $pNew_cases;
        $this -> cumulative_cases = $pCumulative_cases;
        $this -> new_deaths = $pNew_deaths;
        $this -> cumulative_deaths = $pCumulative_deaths;
        $this -> id_country_country = $pId_country_country;
        $this -> conexion = new Conexion();
        $this->reportDAO=new ReportDAO($pId_report, $pDate, $pNew_cases, $pCumulative_cases, $pNew_deaths, $pCumulative_deaths, $pId_country_country);
    }
    
    function consultaID($param) {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->reportDAO->consultaID($param));
        $this->conexion->cerrar();
        $reporte = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $pais = new Country();
            $pais -> buscaID($resultado[6]);
            array_push($reporte, new Report($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $pais));
        }
        return $reporte;
    }
}

?>