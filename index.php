<?php
require "logica/Country.php";
require "logica/Region.php";
require "logica/Report.php";

$pid = null;
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}

?>

<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>itiud_icai-s</title>
        <script type="text/javascript">
        $(function () {
        	  $('[data-toggle="tooltip"]').tooltip()
        	})
        </script>
    </head>
    
    <body>
        <?php
        
        if ($pid != ""){
            $pid = base64_decode($_GET["pid"]);
            include $pid;
        }else{
            include "presentacion/buscarPais.php";
        }
        
        ?>
    </body>

</html>


